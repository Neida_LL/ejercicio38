import { Component, OnInit } from '@angular/core';
import { Ipersona } from "../../interfaces/persona.interface";

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {


  persona:Ipersona ={
    nombre:'',
    direccion:''
    
  }

  constructor() { }

  ngOnInit(): void {
  }

  guardar():void {
    console.log(this.persona.nombre);
    console.log(this.persona.direccion);
  }
}
